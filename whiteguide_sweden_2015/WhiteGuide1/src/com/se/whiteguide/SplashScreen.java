package com.se.whiteguide;

import java.util.Vector;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.live.se.whiteguide.R;
import com.se.whiteguide.dataengine.DataEngine;
import com.se.whiteguide.dataengine.DataHandler;
import com.se.whiteguide.dataengine.InitialInformation;
import com.se.whiteguide.dataengine.PaymentInformation;
import com.se.whiteguide.helpers.BasicInfoBean;
import com.se.whiteguide.log.ExceptionHandlerForceClose;
import com.se.whiteguide.log.MyDebug;
import com.se.whiteguide.parsers.BasicDataInfo;
import com.se.whiteguide.parsers.RestaurantsParser;
import com.se.whiteguide.pushnotifications.CommonUtilities;
import com.se.whiteguide.tabgroup.Home;

/***
 * Represents the Splash Screen application The Image will display upto 2seconds
 * and then the Home screen will be launched. If the User opened very first
 * time..Then the account picker alert will be shown to select email id.
 * 
 * @author Conevo
 * 
 */

public class SplashScreen extends Activity {
	// Handler to post the Runnable on UI thread.
	Handler handler;
	// ImageView to show the SplashImage.
	ImageView splashImage;
	// Intent to launch the next Activity{Home Screen/User-Guide Screens}
	Intent intent;
	// To check whether the screen is alive (or) not.
	boolean isAlive = false, started = false;
	// Time to show the Image
	private static final int PICK_ACCOUNT_REQUEST = 1000;
	// Screen height & width
	int height, width;
	// To know whether the app was opened first time/not
	SharedPreferences settings;
	// class to validate user and do parsing
	DataLoader dataloader;
	// progress dialog shown until data is loaded
	ProgressDialog progress;
	// To get restaurant data
	private RestaurantsParser res_parser;
	// classes importing
	DataHandler dataHandler;
	private InitialInformation information;
	PaymentInformation payInfo;
	private BasicDataInfo dataInfo;
	private Vector<BasicInfoBean> beanObjList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// Set the Content
		setContentView(R.layout.activity_main);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandlerForceClose(
				this));
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			if (extras.containsKey(DataEngine.ERROR_MSG))
				MyDebug.addCrashReport(extras.getString(DataEngine.ERROR_MSG));
		}

		splashImage = (ImageView) findViewById(R.id.splash);

		// Display Metrics object to get the screen Resolution.
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		// Activity is alive
		isAlive = true;
		// Height & width of the Screen.
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;

		settings = getSharedPreferences(DataEngine.SETTINGS_NAME, MODE_PRIVATE);
		beanObjList = new Vector<BasicInfoBean>();
		information = new InitialInformation(this);
		payInfo = new PaymentInformation(this);
		beanObjList = information.readData();
		dataHandler = new DataHandler(getApplicationContext());
		dataloader = new DataLoader();
		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);

		if (payInfo.isAtleastOneguideBought()
				|| information.isAtleastOneguideBought()) {
			DataEngine.showAds = false;
		}

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		// GCMRegistrar.checkManifest(this);
		GCMRegistrar.register(this, CommonUtilities.SENDER_ID);

		progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.loading));

		// Initialize the Handler
		handler = new Handler();

		if (!NetworkChecker.isNetworkOnline(this)
				&& dataHandler.getDataCount(beanObjList) <= 0) {
			showErrorDialog();
			return;
		} else {
			// nothing to do...
		}
		if (settings.getString(DataEngine.SETTINGS_MAIL, null) != null) {
			setSplashImage();
		} else {
			showGoogleAccountPicker();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PICK_ACCOUNT_REQUEST && resultCode == RESULT_OK) {
			String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
			Editor editor = settings.edit();
			editor.putString(DataEngine.SETTINGS_MAIL, accountName.trim());
			editor.commit();
			setSplashImage();
		} else {
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * shows dialog when there is no internet
	 */
	private void showErrorDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.networkerror));
		builder.setTitle(getString(R.string.warning));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
		if (isAlive)
			builder.show();
	}

	/**
	 * sets splash image according to screen resolution
	 */
	private void setSplashImage() {
		if (width > 240 && width <= 320) {
			splashImage.setImageResource(R.drawable.splash_320);
		} else if (width > 320 && width <= 480) {
			splashImage.setImageResource(R.drawable.splash_480);
		} else if (width > 480 && width <= 640) {
			splashImage.setImageResource(R.drawable.splash_640);
		} else if (width > 640 && width < 800) {
			splashImage.setImageResource(R.drawable.splash_720);
		} else if (width == 800) {
			splashImage.setImageResource(R.drawable.splash_800);
		} else {
			splashImage.setImageResource(R.drawable.splash_800);
		}

		dataloader = new DataLoader();
		dataloader.execute();

	}

	/**
	 * Runnable which handles the logic to start new Activity after some
	 * specified time(2sec).
	 */
	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			if (isAlive && !started) {
				started = true;
				startActivity(new Intent(SplashScreen.this, Home.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			}
			finish();
		}
	};

	@Override
	protected void onResume() {
		isAlive = true;
		super.onResume();
	}

	@Override
	protected void onPause() {
		isAlive = false;
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		isAlive = false;
		if (null != handler) {
			handler.removeCallbacks(runnable);
		}
		super.onDestroy();
	}

	/**
	 * class to check valid user, network error and force update when current
	 * version differs
	 */
	class DataLoader extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				if (progress != null) {
					if (!progress.isShowing()) {
						progress.show();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			validateUser();
			try {
				doParsing();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			int versionCode = 0;
			int minver = 0;
			int maxver = 0;

			try {
				versionCode = getPackageManager().getPackageInfo(
						getPackageName(), 0).versionCode;
				maxver = versionCode;
				if (beanObjList.size() != 0) {
					minver = beanObjList.get(0).getMinimumversion();
					maxver = beanObjList.get(0).getMaximumversion();
				}
			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}

			if (minver <= versionCode && versionCode <= maxver) {
				handler.postDelayed(runnable, 0);
				if (progress != null) {
					if (progress.isShowing()) {
						progress.dismiss();
					}
				}

				if (dataHandler.getDataCount(beanObjList) > 0) {
					handler.post(runnable);
				} else {
					Toast.makeText(SplashScreen.this, R.string.networkerror,
							Toast.LENGTH_SHORT).show();
				}
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						SplashScreen.this);
				builder.setMessage(R.string.force_update);
				builder.setTitle(R.string.warning);
				builder.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								final String my_package_name = SplashScreen.this
										.getPackageName(); // <- HERE YOUR
															// PACKAGE NAME!!
								String url = "";
								try {
									url = getString(R.string.market_link)+ my_package_name;
								} catch (final Exception e) {
									url = getString(R.string.browser_link)+ my_package_name;
								}
								Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
								startActivity(myIntent);
								finish();
							}
						});
				if (isAlive)
					builder.show();
			}

			super.onPostExecute(result);
		}
	}

	/**
	 * parsing url to get entire data
	 */
	public void doParsing() {
		for (BasicInfoBean infoBean : beanObjList) {
			MyDebug.log_info("Error", "MY GUIDE: ", infoBean.getGuideName());
			if (dataHandler.getDataCount(infoBean) > 0) {
				ParserThread newThread = new ParserThread(infoBean);
				newThread.start();
			} else {
				if (infoBean.getGuideId().equalsIgnoreCase("90")) {
					res_parser = new RestaurantsParser(SplashScreen.this,
							DataEngine.BASE_URL + DataEngine.NYTESTAT_INFO,
							infoBean.getGuideName(), infoBean.getGuideId(),
							infoBean.getGuideYear());
				} else {
					res_parser = new RestaurantsParser(SplashScreen.this,
							DataEngine.BASE_URL
									+ DataEngine.ALL_RESTAURANTS_BASIC_INFO
									+ infoBean.getGuideId(),
							infoBean.getGuideName(), infoBean.getGuideId(),
							infoBean.getGuideYear());
				}
				res_parser.doJsonparse();

			}

		}

	}

	/**
	 * called when data handler count is greater than 0
	 */
	class ParserThread extends Thread {
		BasicInfoBean beanObj;

		public ParserThread(BasicInfoBean bean) {
			beanObj = bean;
		}

		@Override
		public void run() {
			try {
				res_parser = new RestaurantsParser(SplashScreen.this,
						DataEngine.BASE_URL
								+ DataEngine.ALL_RESTAURANTS_BASIC_INFO
								+ beanObj.getGuideId(), beanObj.getGuideName(),
						beanObj.getGuideId(), beanObj.getGuideYear());
				res_parser.doJsonparse();
			} catch (Exception e) {
				e.printStackTrace();
			}
			super.run();
		}

	}

	/**
	 * account picker to select email id
	 */
	private void showGoogleAccountPicker() {
		Intent googlePicker = AccountPicker.newChooseAccountIntent(null, null,
				new String[] { GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE }, true,
				null, null, null, null);
		startActivityForResult(googlePicker, PICK_ACCOUNT_REQUEST);
	}

	/**
	 * to validate user whether he has bought the guide or not
	 */
	public void validateUser() {
		for (BasicInfoBean tempObj : payInfo.readData()) {
			if (tempObj.isGuideisBought()) {
				DataEngine.showAds = false;
				break;
			}
		}
		dataInfo = new BasicDataInfo(getApplicationContext());
		dataInfo.doParsing();
		beanObjList = information.readData();
		MyDebug.log_info("Error", "THE SIze here is:",String.valueOf(beanObjList.size()));
	}
}
