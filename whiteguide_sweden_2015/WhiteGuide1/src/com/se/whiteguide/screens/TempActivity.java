package com.se.whiteguide.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.se.whiteguide.tabgroup.Home;

/**
 * Represents the view from the notification form server
 * @author Conevo 
 */


public class TempActivity extends Activity{
	String news_id=null;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Bundle extras=getIntent().getExtras();
	if(extras!=null){
		news_id=extras.getString("NEWSID");
		Log.e("PUSHNOTIFICATION","--------push bundle--------"+extras+"-------"+news_id);
	}
	if(news_id==null){
	Intent notificationIntent = new Intent(this,Home.class);
	// set intent so it does not start a new activity
	notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
			| Intent.FLAG_ACTIVITY_SINGLE_TOP);
	notificationIntent.putExtras(getIntent().getExtras());
	startActivity(notificationIntent);
	finish();
	}else{
		Intent newsNotificationIntent = new Intent(this,DetailedNewspush.class);
		newsNotificationIntent.putExtras(getIntent().getExtras());
		newsNotificationIntent.putExtra("newsid",news_id);
		startActivity(newsNotificationIntent);
		finish();
	}
}
}
