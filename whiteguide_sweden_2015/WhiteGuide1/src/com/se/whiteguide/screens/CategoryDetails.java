package com.se.whiteguide.screens;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.RelativeLayout;

import com.live.se.whiteguide.R;



/**
 *  Represents entire category details with animation 
 *  @author Conevo
 */
public class CategoryDetails extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.category_detail);
		RelativeLayout cat_detail = (RelativeLayout) findViewById(R.id.header_tab);
		cat_detail.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
			    overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
				finish();
				return true;
			}
		});

	}

}
