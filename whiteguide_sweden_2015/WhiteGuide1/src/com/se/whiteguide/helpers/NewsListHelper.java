package com.se.whiteguide.helpers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class NewsListHelper implements Serializable {
	private String totalrowss;
	private List<NewsListHelper.NewsHelper> news_data = new ArrayList<NewsListHelper.NewsHelper>();

	public List<NewsListHelper.NewsHelper> getNews_data() {
		return news_data;
	}

	public void setNews_data(List<NewsListHelper.NewsHelper> news_data) {
		this.news_data = news_data;
	}

	public String getTotalrowss() {
		return totalrowss;
	}

	public void setTotalrowss(String totalrowss) {
		this.totalrowss = totalrowss;
	}

	public class NewsHelper implements Serializable {
		private String id, changed, title, date, imagelink, smalldesc,
				fulldesc, itemurl;

		public String getItemurl() {
			return itemurl;
		}

		public void setItemurl(String itemurl) {
			this.itemurl = itemurl;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getChanged() {
			return changed;
		}

		public void setChanged(String changed) {
			this.changed = changed;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getImagelink() {
			return imagelink;
		}

		public void setImagelink(String imagelink) {
			this.imagelink = imagelink;
		}

		public String getSmalldesc() {
			return smalldesc;
		}

		public void setSmalldesc(String smalldesc) {
			this.smalldesc = smalldesc;
		}

		public String getFulldesc() {
			return fulldesc;
		}

		public void setFulldesc(String fulldesc) {
			this.fulldesc = fulldesc;
		}
	}

}
