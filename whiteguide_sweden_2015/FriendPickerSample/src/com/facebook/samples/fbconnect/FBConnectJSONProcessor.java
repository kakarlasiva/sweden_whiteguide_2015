package com.facebook.samples.fbconnect;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;

public class FBConnectJSONProcessor {

	public List<FacebookAlbumInfoBean> getAlbumList(Context context,
			String albumListResponse, String taggedPhotoResponse,
			String appAlbumName) throws JSONException {
		List<FacebookAlbumInfoBean> listFacebookAlbumInfoBean = new ArrayList<FacebookAlbumInfoBean>();
		FacebookAlbumInfoBean mFacebookAlbumInfoBean;
		listFacebookAlbumInfoBean = new ArrayList<FacebookAlbumInfoBean>();
		JSONObject jsonObject = new JSONObject(albumListResponse);
		if (jsonObject.has("data")) {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			if (jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject object = jsonArray.getJSONObject(i);
					mFacebookAlbumInfoBean = new FacebookAlbumInfoBean();
					if (object.has("aid")) {
						mFacebookAlbumInfoBean.setAlbumID(object
								.getString("aid"));
					}
					if (object.has("name")) {
						mFacebookAlbumInfoBean.setAlbumName(object
								.getString("name"));
					}
					if (object.has("cover_object_id")) {
						mFacebookAlbumInfoBean.setAlbumCoverPhotoID(object.get(
								"cover_object_id").toString());
						mFacebookAlbumInfoBean
								.setAlbumCoverPhotoURL(Util.IMAGELINK
										+ object.getString("cover_object_id")
										+ Util.ALBUM_COVER_PIC
										+ SessionStore.getAccessTocken(context));
					}
					if (object.getInt("photo_count") > 0) {
						mFacebookAlbumInfoBean.setAlbumPicsCount(object.get(
								"photo_count").toString());
						if (!mFacebookAlbumInfoBean.getAlbumName()
								.equalsIgnoreCase(appAlbumName))
							listFacebookAlbumInfoBean
									.add(mFacebookAlbumInfoBean);
					}
				}
			}
		}
		JSONObject tagsjsonObject = new JSONObject(taggedPhotoResponse);
		if (tagsjsonObject.has("data")) {
			JSONArray tagjsonArray = tagsjsonObject.getJSONArray("data");
			if (tagjsonArray.length() > 0) {
				mFacebookAlbumInfoBean = new FacebookAlbumInfoBean();
				JSONObject obj = tagjsonArray.getJSONObject(0);
				mFacebookAlbumInfoBean.setAlbumID(Util.TAGGED_PHOTOS_ID);
				mFacebookAlbumInfoBean.setAlbumCoverPhotoURL(Util.IMAGELINK
						+ obj.get("object_id")
						+ "/picture?type=thumbnail&access_token="
						+ SessionStore.getAccessTocken(context));
				mFacebookAlbumInfoBean.setAlbumPicsCount(""
						+ tagjsonArray.length());

				mFacebookAlbumInfoBean.setAlbumName("Photos of Me");
				listFacebookAlbumInfoBean.add(mFacebookAlbumInfoBean);
			}

		}

		return listFacebookAlbumInfoBean;
	}

	public List<FBAlbumGridInfoBean> getPhotoList(Context context,
			String response) throws JSONException {
		List<FBAlbumGridInfoBean> listFBAlbumGridInfoBean = new ArrayList<FBAlbumGridInfoBean>();
//		JSONArray itemList;
//		JSONObject rootObj = new JSONObject(response);
//		itemList = rootObj.getJSONArray("data");
//		rootObj = null;
//		if (itemList.length() > 0) {
//			for (int i = 0; i < itemList.length(); i++) {
//				FBAlbumGridInfoBean fbAlbumGridInfoBean = new FBAlbumGridInfoBean();
//				JSONObject object = itemList.getJSONObject(i);
//				if (object.has("images")) {
//					JSONArray jsonArray = object.getJSONArray("images");
//					// fbAlbumGridInfoBean.setSourceUrl(jsonArray.getJSONObject(0).get("source").toString());
//				}
//				if (object.has("src_big")) {
//					fbAlbumGridInfoBean.getPrintUrl(object.get("src_big")
//							.toString());
//				}
//				if (object.has("src_small")) {
//					fbAlbumGridInfoBean.setPictureUrl(object.get("src_small")
//							.toString());
//				}
//				if (object.has("object_id")) {
//					fbAlbumGridInfoBean.setId(object.get("object_id")
//							.toString());
//				}
//				if (object.has("src")) {
//					fbAlbumGridInfoBean.setSourceURL(object.get("src")
//							.toString());
//				}
//				if (object.has("src_width")) {
//					fbAlbumGridInfoBean.setSourceWidth(object
//							.getInt("src_width"));
//				}
//				if (object.has("src_height")) {
//					fbAlbumGridInfoBean.setSourceHeight(object
//							.getInt("src_height"));
//				}
//				fbAlbumGridInfoBean.setSelected(false);
//				fbAlbumGridInfoBean.setUploaded(false);
//				listFBAlbumGridInfoBean.add(fbAlbumGridInfoBean);
//			}
//
//		}
		return listFBAlbumGridInfoBean;
	}
}
